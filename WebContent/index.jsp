<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chat Application</title>
    <!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">    <style>
        #box {
            height: 300px;
            resize: none;
        }
    </style>
</head>
<body>
    <div class="container mt-5">
        <div class="card">
            <div class="card-header text-center">
                <h3>Chat Application</h3>
                <span id="total_client" class="ms-3"></span>
            </div>
            <div class="card-body">
                <textarea id="box" class="form-control mb-3" readonly></textarea>
                <div class="input-group">
                    <input type="text" id="input" class="form-control" placeholder="Type your message..."/>
                    <button class="btn btn-primary" onclick="send()">Send</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap JS and dependencies -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.10.2/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.1.3/js/bootstrap.min.js"></script>
	<script>
		let wsServer = new WebSocket('ws://localhost:8181');
		let username = prompt("Enter your name");
		wsServer.onopen = function(message)
		{
			let msg = username + " joined";
			if(wsServer.readyState == WebSocket.OPEN)
			{
				wsServer.send(msg);
			}
		}
		wsServer.onmessage = function(message)
		{
			let dataObject = JSON.parse(message.data);
			if(dataObject.type == 'message')
				box.value += dataObject.content + '\n';
			else if(dataObject.type == 'count')
				total_client.innerHTML = "Total Client: " + dataObject.total;
		}
		wsServer.onclose = function(message)
		{
			box.value = username + " leaved";
		}
		wsSever.onerror = function(message)
		{
			alert("Error: " + message);
		}
		function send(){
			let msg = input.value;
			if(wsServer.readyState == WebSocket.OPEN)
			{
				wsServer.send(msg);
				input.value = '';
			}
		}
	</script>
</body>
</html>

