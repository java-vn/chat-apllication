var WebSockerServer = require('websocket').server;
var http = require('http');
var clients = [];
var port = 8181;
var server = http.createServer(function(req,res){
    console.log(req.url);
    if(req.method === 'GET' && req.url === '/clients-count'){
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify({total: clients.length}));
        
    }
});
server.listen(port, function() {
    console.log(`Server is listening on port ${port}`);
});
wsServer = new WebSockerServer({httpServer: server});
wsServer.on('request', function(req){
    var connection = req.accept(null, req.origin);
    clients.push(connection);
    broadcastClientCount();
    connection.on('message', msg => {
        if(msg.type === 'utf8')
        {
            for(var i=0; i < clients.length; i++)
            {
                var messageData = JSON.stringify({
                    type: 'message',
                    content: msg.utf8Data
                });
                for (var i = 0; i < clients.length; i++) {
                    clients[i].sendUTF(messageData);
                }  
            }
        }
    });
    connection.on('close', function(){
        // Loại bỏ client khi kết nối đóng
        clients = clients.filter((c) => {
            return c !== connection;
        })
        broadcastClientCount();
    });
});
// Hàm gửi số lượng client hiện tại cho tất cả các client
function broadcastClientCount() {
    var message = JSON.stringify({
        type: 'count',
        total: clients.length
    });
    for (var i = 0; i < clients.length; i++) {
        clients[i].sendUTF(message);
    }
}